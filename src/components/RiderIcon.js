import React from 'react';
import {
  View, Image
} from 'react-native';
import {
   Text
} from 'native-base'
const styles = {
  container: {
    backgroundColor: "transparent",
    margin: 3,
    marginLeft: 70,
    padding: 10 ,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,
    elevation: 20,

  },
  innerContainer: {
    flex: 1, flexDirection: 'row',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,
    elevation: 20,
  },
  iconContainer: {
    backgroundColor: '#fff',
     height: 40,
     width: 40,
     justifyContent: 'center',
     alignItems: 'center',
     borderLeftRaidus: 30,
  },
  durationContainer: {
    backgroundColor: '#000',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  duration: {
    fontSize: 12,
    color: '#fff',
    alignSelf: 'center'
  }
};
export default RiderIcon = ({ icon, duration }) => {
  return (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <View style={styles.iconContainer}>
          <Image source={{ uri: icon }} style={{ height: 30, width: 30 }} />
        </View>
        <View style={styles.durationContainer}>
          <Text style={styles.duration}>
            {Math.round(duration)} mins
                  </Text>
        </View>
      </View>
    </View>
  );
}

