import React from 'react';
import {
  View, Text, Spinner
} from 'native-base';
import {StyleSheet} from "react-native";

class Splash extends React.Component {
  render(){
    return (
      <View style={styles.container}>
        <Spinner color="blue"/>
        <Text note>
          Loading
        </Text>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4A4A4A',
    justifyContent: 'center',
    alignItems: 'center'
  },
});

export default Splash;