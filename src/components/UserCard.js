import React, {Component} from 'react';
import {
    View, Image, Text, Linking
} from 'react-native';
import {
  Button, Icon
} from 'native-base';
import {styles } from '../components/styles'
import moment from 'moment'

const staticAvatar = 'https://randomuser.me/api/portraits/men/83.jpg';

export default class UserCardComponent extends Component {
    componentDidMount(){

    }

    _onCall = (phone) => {
      Linking.openURL(`tel:${phone}`);
      return true;
    };

    render() {
        let user  = this.props.user;

        return (
            <View style={[ styles.row, { padding: 5, height: 100}]}>
                <View style={[ styles.box, styles.circle, {width: 70, height: 70}]}>
                <Image
                        style={[styles.circle, {width: 70, height: 70}]}
                        source={{ uri: staticAvatar }}
                    />
                </View>
                <View style={[ styles.box, { flex: 2, paddingLeft: 5, justifyContent: 'center', alignItems: 'center', }]}>
                    <Text style={[styles.h3, styles.textPrimary]}>{user && user.name}</Text>
                    <Text style={[ { fontSize: 12}]} > {user.phone}</Text>
                </View>
                <View style={[ styles.box, { justifyContent: 'center', alignItems: 'center', }]}>
                  <Button succeess rounded block onPress={() => this._onCall(user.phone)}>
                    <Icon type={"MaterialCommunityIcons"}  name="phone"  size={25}/>
                  </Button>
                </View>
            </View>
        );
    }
}



