import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Text, TextInput, View, StyleSheet } from 'react-native';

export default class PinInput extends Component {
  static propTypes = {
    codeLength: PropTypes.number,
    pinValueLength: PropTypes.number,
    secureTextEntry: PropTypes.bool,
    placeHolder: PropTypes.string
  };

  state  = {
    value: {},
    input: {
      borderBottomWidth: 1,
      borderBottomColor: '#CCC'
    },
    activeInput: {
      borderBottomWidth: 2,
      borderBottomColor: '#000'
    },
    focus: null
  };

  getValue = (index) => {
    val = "";
    try {
      val =  this.state.value[index - 1]
    } catch(e) {
    }

    return val;
  };

  onEntry  = (index, value) => {
    let next  = index+1;
    let prev  =  index-1;

    this.setValue(index, value);
    this.move(index);
    if(this.props.hasOwnProperty('onEntry')){
      this.props.onEntry(this.getString())
    }
  };

  setValue = (index, value) => {
    index  = index -1 ;
    let _value  =  this.state.value;
    _value[index] = value;
    this.setState({ value: _value})
  };

  onFocus = (index) => {
    this.setState({
      focus: index
    })
  };

  getString = () => {
    str = ''
    for(var i = 0;i < this.props.codeLength; i++){
      val = this.getValue(i + 1);
      if(val)
        str = str + val
    }

    return str;
  };

  onBlur = (index) => {

  };

  move =  (index) => {
    let next  = index+1;
    let prev  =  index-1;
    const value  = this.getValue(index);

    if(!value){
      prev = prev <= 1 ? 1: prev;
      return this.refs[`pinInput${prev}`].focus()
    }else{
      next = next >= this.props.codeLength ? this.props.codeLength: next;
      return this.refs[`pinInput${next}`].focus()
    }
  };

  _onKeyPress = (e, index) => {
    if(e.nativeEvent.key){
      this.move(index)
    }
  };

  render() {
    return (
      <View
        style={[styles.pinBoxList, this.props.containerStyle]}>
        {this.renderPills()}
      </View>
    );
  }

  renderPills() {
    let pills = [];

    for (var i = 0; i < this.props.codeLength; i++) {
      pills.push(this.renderPill(i + 1));
    }

    return pills;
  }

  renderPill(index) {
    return (
      <TextInput
        key={index}
        ref={`pinInput${index}`}
        value={() => this.getValue(index)}
        placeholder={`${index}`}
        secureTextEntry={this.props.secureTextEntry}
        onChangeText={(value) => this.onEntry(index, value)}
        style={[styles.pinBox, this.state.focus === index ? this.state.activeInput : this.state.input]}
        onBlur={() => this.onBlur(index)}
        onFocus={() => this.onFocus(index)}
        maxLength={1}
        onKeyPress={(event) => this._onKeyPress(event, index)}
        selectTextOnFocus={true}
        keyboardType={this.props.keyboardType}
        underlineColorAndroid='transparent'
        autoCorrect={false}
      />
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  input: {
    backgroundColor: 'red',
    position: 'absolute',
    right: -99,
  },
  pinBox: {
    alignItems: 'center',
    height: 30,
    width: 30,
    marginRight: 14,
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold'
  },
  pinBoxList: {
    flex: -1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 20,
  },
  pinView: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: 'rgb(239, 239, 244)',
  },
  pinPromptText: {
    marginBottom: 10,
  }
});
