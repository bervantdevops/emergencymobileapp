import service from '../module/userService'

export default {
  state: {
    history: [],
    current: null,
    responderLocation: null,
  }, // initial state
  reducers: {
    // handle state changes with pure functions
    set(state, payload) {
      return {...state, history: payload};
    },
    add(state, payload){
      return {...state, current: payload}
    },
    cancel(state){
      return {...state, current: null }
    },
    update(state, currentRequest){
      return {...state, current: currentRequest }
    },
    location(state, data){
      return {...state, responderLocation: data }
    }
  },
  effects: (dispatch) => ({
    async all(params, rootState) {
      return service.myContacts(params).then( data => {
        dispatch.requests.set(data.data);
      });
    },
    async makeNewRequest(payload, rootState) {
      return service.makeRequest(payload).then( data => {
        dispatch.requests.add(data.data);
        dispatch.requests.location(data.data.reponder.location);
      });
    },
    async cancelRequest(id, rootState) {
      return service.cancelRequest(id).then( data => {
        dispatch.requests.cancel();
      });
    },
    async updateRequest(payload, rootState) {
      dispatch.requests.update(payload);
    },
    async updateResponderLocation(payload, rootState) {
      dispatch.requests.location(payload);
    }
  })
}
