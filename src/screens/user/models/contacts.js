import service from '../module/userService'

export default {
  state: [], // initial state
  reducers: {
    // handle state changes with pure functions
    set(state, payload) {
      return payload;
    },
    add(state, payload){
      return [...state, payload]
    },
    update(state, id, payload){
      return state.map( b => {
        if(b.id === id)
          return {...b, payload};

        return b;
      })
    },
    remove(state, pos){
      return state.filter((b, index) => pos != index)
    }
  },
  effects: (dispatch) => ({
    async all(params, rootState) {
      return service.myContacts(params).then( data => {
        dispatch.contacts.set(data.data);
      });
    },
    async addContact(payload, rootState) {
      return service.addContact(payload).then( data => {
        dispatch.contacts.add(data.data);
      });
    },
    async updateContact(id, payload, rootState) {
      return service.editContact(payload).then( data => {
        dispatch.contacts.update(id, data.data);
      });
    },
    async removeContact(id, rootState, index) {
      return service.removeContact(id).then( data => {
        dispatch.contacts.remove(index);
      });
    },
  })
}