import {connect} from "react-redux";
import Pricing from '../components/pricing';

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  subscribe: (params) => dispatch.user.subscribe(params),
});

export default connect(mapStateToProps, mapDispatchToProps)(Pricing);
