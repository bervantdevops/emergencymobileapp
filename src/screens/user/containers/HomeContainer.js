import {connect} from "react-redux";
import Home from '../components/home';
const mapStateToProps = state => ({
  user: state.user,
  // requests: state.requests.history,
  currentRequest: state.requests.current,
  responderLocation: state.requests.responderLocation
});

const mapDispatchToProps = dispatch => ({
  makeRequest: (payload) => dispatch.requests.makeNewRequest(payload),
  cancelRequest: (request_id) => dispatch.requests.cancelRequest(request_id),
  updateRequest: (request) => dispatch.requests.updateRequest(request),
  updateResponderLocation: (location) => dispatch.requests.updateResponderLocation(location),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
