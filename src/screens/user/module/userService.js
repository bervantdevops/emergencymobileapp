import { Helpers } from 'my-components'
import axios from 'axios'
import { store } from '../../../store/rematch'

export default {
  registerPushToken: async (token) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url('profile/push-token'), {push_token: token}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  myProfile: async (payload) => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url('profile'), {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  getCounties: async () => {
    return axios.get(Helpers.url('counties'),)
      .then( resp => {
        return resp.data;
      })
  },
  updateProfile: async (payload) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url('profile/update'),payload, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },

  getUserProfile: async (user_id) => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url(`users/${user_id}/profile`), {headers: auth}).then(resp => {
      return resp.data;
    })
  },

  myContacts: async () => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url(`contacts`), {headers: auth}).then(resp => {
      return resp.data;
    })
  },
  addContact: async (payload) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url('contacts/new'),payload, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  editContact: async (id, payload) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`contacts/${id}`), payload, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  removeContact: async (id) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`contacts/${id}/remove`), {}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  myRequests: async () => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url(`emergencies`), {headers: auth}).then(resp => {
      return resp.data;
    })
  },
  makeRequest: async (payload) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url('emergencies/new'),payload, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  cancelRequest: async (id) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`emergencies/${id}/cancel`), {}, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  subscribe: async (params) => {
    const auth  =  await auth_header();
    return axios.post(Helpers.url(`subscribe`), params, {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  myCategories: async () => {
    const auth  =  await auth_header();
    return axios.get(Helpers.url(`categories`), {headers: auth})
      .then( resp => {
        return resp.data;
      })
  },
  getAllCategories: async () => {
    return axios.get(Helpers.url('services'),)
      .then( resp => {
        return resp.data;
      })
  },
  getPlans: async () => {

    return axios.get(Helpers.url('pricing-plans'),)
      .then( resp => {
        return resp.data;
      })
  },
}

async function auth_header() {
  const  { user } = await store.getState();

  if (user && user.accessToken) {
    return {
      'Authorization': 'Bearer ' + user.accessToken
    };
  } else {
    return {};
  }
}
