import React, {Component} from "react";
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  FlatList
} from "react-native";

import {
  Container, Content, Icon, Header,
  Left, Body, Right, Segment, Button,
  Title, Spinner, List, Text
} from 'native-base'
import {Theme} from 'my-components'

var {height, width} = Dimensions.get('window');
import userService from '../../module/userService'


class MyProfile extends Component {
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: ({tintColor}) => (<Icon name={"home"}/>)
  };

  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0,
      user: null,
      ready: false,
      posts: [],
      sending: false
    }

  };

  componentDidMount = () => {
    this.getUserProfile();
  };

  getUserProfile = () => {
    const {user}  = this.props;
    this.setState({ready: false});
    userService.getUserProfile(user.pk).then(data => {
      this.setState({
        user: data.data,
        posts: data.posts
      })
    }).catch(error => {
      console.error(error);
    }).finally(() => {
      this.setState({ready: true});
    })
  };

  segmentClicked(index) {
    this.setState({
      activeIndex: index
    })
  }

  checkActive = (index) => {
    if (this.state.activeIndex !== index) {
      return (
        {color: 'grey'}
      )
    }
    else {
      return (
        {}
      )
    }

  };
  logout = async () => {
    await this.props.logout();
    this.props.navigation.navigate('Auth');
  };



  back = () => {
    this.props.navigation.goBack();
  };

  settings = () => {
    this.props.navigation.navigate('Settings')
  };


  componentDidMount() {
    console.log(width)
  }

  render() {
    const {user } = this.props;
    const { ready} = this.state;
    return (
      <Container style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'} style={{paddingLeft: 10}}>
          <Left>
            <Button transparent onPress={this.back}>
              <Icon ios="ios-arrow-back" android="md-arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>My Account</Title>
          </Body>
          <Right/>
        </Header>
        {ready ? this.renderContent() : this.renderLoading()}
      </Container>
    );
  }

  renderLoading() {
    return (
      <Content contentContainerStyle={styles.loadingContainer}>
        <Spinner color='blue'/>
      </Content>
    );
  }

  renderUserActions() {
    const {user} = this.state;
    const me = this.props.user;
    return (
      <View
        style={{flexDirection: 'row', paddingHorizontal: 5}}>

        <Button bordered dark
                style={{flex: 3, marginLeft: 10, justifyContent: 'center', height: 30}}><Text>Edit Profile</Text>
        </Button>
        <Button
          onPress={this.logout}
          bordered
          dark
          style={{
          flex: 1,
          height: 30,
          marginRight: 10, marginLeft: 5,
          justifyContent: 'center'
        }}>
          <Icon ios="ios-unload" android="md-unlock" style={{color: 'black', fontSize: 15}}/>
        </Button>
      </View>
    );
  }

  renderContent() {
    const {user} = this.state;
    return (
      <Content>

        <View style={{paddingTop: 10}}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
              <Image source={{uri: user.avatar_url}}
                     style={{width: 75, height: 75, borderRadius: 37.5}}/>

            </View>
            <View style={{flex: 3}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'flex-end'
                }}>
                <View style={{alignItems: 'center'}}>
                  <Text>{user.total_posts}</Text>
                  <Text style={{fontSize: 10, color: 'grey'}}>Posts</Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <Text>{user.total_followers}</Text>
                  <Text style={{fontSize: 10, color: 'grey'}}>Followers</Text>
                </View>
                <View style={{alignItems: 'center'}}>
                  <Text>{user.total_followings}</Text>
                  <Text style={{fontSize: 10, color: 'grey'}}>Following</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'flex-start', paddingTop: 10}}>
                {this.renderUserActions()}
              </View>{/**End edit profile**/}
            </View>
          </View>

          <View style={{paddingBottom: 10, paddingTop: 10}}>
            <View style={{paddingHorizontal: 10}}>
              <Text style={{fontWeight: 'bold'}}>{user.name}</Text>
            </View>
          </View>


        </View>


        <View>
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            borderTopWidth: 1,
            borderTopColor: '#eae5e5'
          }}>
            <Button

              onPress={() => this.segmentClicked(0)}
              transparent
              active={this.state.activeIndex == 0}

            >
              <Icon name="ios-apps-outline"
                    style={[this.state.activeIndex == 0 ? {} : {color: 'grey'}]}>
              </Icon>
            </Button>
            <Button
              onPress={() => this.segmentClicked(1)}
              transparent active={this.state.activeIndex == 1}>
              <Icon name="ios-list-outline"
                    style={[{fontSize: 32}, this.state.activeIndex == 1 ? {} : {color: 'grey'}]}/>
            </Button>
          </View>


        </View>
      </Content>
    );
  }
}

export default MyProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});