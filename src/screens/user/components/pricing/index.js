import React from 'react'
import {Platform, StyleSheet, Image} from 'react-native'
import {
  Container, Header, Left, Right, Title, Body, Content, View, Text,
  Button, Item, Icon, Spinner, Fab, List, ListItem, Form, Radio
} from 'native-base';
import styles from './pricingStyles'
import Modal from 'react-native-modal'
import {Helpers} from "my-components";
import OverlaySpinner from 'react-native-loading-spinner-overlay';
import userService from '../../../user/module/userService'
import PriceCard from './PriceCard';
import SubscribeForm from './SubscribeForm';

export default class Contacts extends React.Component {

  static navigationOptions = {
    drawerLabel: 'Membership',
    drawerIcon: null
  };

  state = {
    loading: false,
    bSending: false,
    bRemoving: false,
    fErrors: {},
    refreshing: false,
    addModalVisible: false,
    plans: [],
    currentPlan: null,
    selected: null,
  };

  componentWillMount = () => {
    const { user } = this.props;
    userService.getPlans()
    .then( ({data}) => {
      this.setState({ plans: data })
    })
  };

  toggleAddModal = (plan) => {
    const { addModalVisible } = this.state;
    this.setState({
      addModalVisible: !addModalVisible,
      selected: plan
    })
  };

  _onSubscribe =  (plan, period) => {
    this.setState({ bSending: true });
    this.props.subscribe({plan_id: plan, period: period})
      .then(() => {
        this.setState({ addModalVisible: false })
      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            this.setState({fErrors: ex.response.data.errors});
            break;
          case 430:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          case 500:
            Helpers.showToast({type: 'error', message: ex.response.data.message});
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({ bSending: false })
      })
  };


  render() {
    const { addModalVisible, plan, plans,fErrors, bSending, bRemoving,selected } = this.state;
    const { contacts } = this.props;
    return (
      <Container>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name='arrow-back'/>
            </Button>
          </Left>
          <Body>
          <Title>Pricing</Title>
          </Body>
          <Right />
        </Header>
        <Content padder style={styles.container} contentContainerStyle={{ alignItems: 'center'}}>
          <OverlaySpinner
            visible={bRemoving}
            textContent={'Removing contact...'}
            textStyle={{ color: '#FFF'}}/>
            {!!plans && plans.map(i => <PriceCard
            key={i.id}
            title={i.name}
            price={`${i.price_per_month}`}
            button_title ='GET STARTED'
            info={i.categories}
            onButtonPress={() => this.toggleAddModal(i)}
            titleColor='#AA00FF'
            priceColor='#000'
            />)}
          <Modal
            isVisible={addModalVisible && !!selected}
            onBackButtonPress={() => this.setState({ addModalVisible: false})}
            onBackdropPress={() => this.setState({ addModalVisible: false})}>
            <View style={styles.modalContent}>
              <SubscribeForm plan={selected} onSubmit={this._onSubscribe} />
            </View>
          </Modal>

        </Content>

      </Container>
    )
  }
}

