import React, {useState} from 'react';
import {
  Container, Header, Left, Right, Title, Body, Content, View, Text,
  Button, Item, Icon, Spinner, Fab, List, ListItem, Form, Radio, H3
} from 'native-base';
import {Helpers} from "my-components";
import OverlaySpinner from 'react-native-loading-spinner-overlay';

import styles from './pricingStyles'

class SubscribeForm extends React.Component {
  state = {
    period: '3',
  };

  submit = () => {
    const {period} = this.state;
    this.props.onSubmit(this.props.plan.id, period)
  };

  setPeriod = (period) => {
    this.setState({ period: period })
  }

  render() {
    const { period } = this.state;
    const {errors, loading, plan} = this.props;
    return (
      <Form style={styles.contactFormContainer}>
        <OverlaySpinner
          visible={loading}
          textContent={'Loading...'}
          textStyle={{ color: '#FFF'}}/>

        <H3>Select subscription cycle</H3>
        <ListItem selected={period == "3"} onPress={() => this.setPeriod(3)} >
              <Left>
                <Text>3 Months (Quarterly) Ksh. {plan.price_per_month}</Text>
              </Left>
              <Right>
                <Radio
                  color={"#f0ad4e"}
                  selectedColor={"#5cb85c"}
                  selected={period == '3'}
                />
              </Right>
            </ListItem>
            <ListItem selected={period == "6"} onPress={() => this.setPeriod(6)}>
              <Left>
              <Text>6 Months (Bi-Annually) Ksh. {plan.price_per_half}</Text>
              </Left>
              <Right>
                <Radio
                  color={"#f0ad4e"}
                  selectedColor={"#5cb85c"}
                  selected={period == '6'}
                />
              </Right>
            </ListItem>
            <ListItem selected={period == "12"} onPress={() => this.setPeriod(12)}>
              <Left>
              <Text>12 Months (Yearly) Ksh. {plan.price_per_year}</Text>
              </Left>
              <Right>
                <Radio
                  color={"#f0ad4e"}
                  selectedColor={"#5cb85c"}
                  selected={period == '12'}
                />
              </Right>
            </ListItem>
        {Helpers.renderError(errors, 'relation')}
        <View style={styles.formActionFooter}>
          <Button primary block onPress={this.submit}>
            <Text>Subscribe</Text>
          </Button>
        </View>
      </Form>
    );
  }
};

export default SubscribeForm;
