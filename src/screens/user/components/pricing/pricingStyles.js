export default {
  container: {
    paddingHorizontal: '10%'
  },
  modalContent: {
    marginTop: '20%',
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  contactFormContainer: {
    minHeight: '30%',
  },
  formActionFooter: {
    marginTop: 10,
    justifyContent: 'flex-end',
    alignContent: 'flex-end'
  }
}
