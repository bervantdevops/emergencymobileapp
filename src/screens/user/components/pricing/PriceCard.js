import React, { Component } from 'react';

import { StyleSheet, Platform, View, Text, TouchableOpacity, Alert, Image } from 'react-native';

import PropTypes from 'prop-types';

class PriceCard extends Component {

  render() {

    return (

      <View style={styles.priceCard_Container}>

        <Text style={[styles.title, { color: this.props.titleColor }]}> {this.props.title} </Text>

        <Text style={[styles.price, { color: this.props.priceColor }]}>
          <Text style={styles.currency}>Ksh. </Text>
           {this.props.price}
           <Text style={styles.currency}>/month</Text>
        </Text>

        {this.props.info.map((item, index) => (
          <Text
            key={index}
            style={styles.price_Info}>
            {item.name}
          </Text>
        ))}

        <TouchableOpacity onPress={this.props.onButtonPress} activeOpacity={0.4} style={styles.price_Button} >

          {!!this.props.iconURL && <Image
            source={{ uri: this.props.iconURL }}
            style={styles.iconStyle} />}

          <Text style={styles.TextStyle}> {this.props.button_title} </Text>

        </TouchableOpacity>

      </View>

    );
  }
}
PriceCard.propTypes =
  {
    title: PropTypes.string,
    price: PropTypes.string,
    titleColor: PropTypes.string,
    priceColor: PropTypes.string,
    info: PropTypes.arrayOf(PropTypes.object),
    button_title: PropTypes.string,
    onButtonPress: PropTypes.func,
    iconURL : PropTypes.string

  }

PriceCard.defaultProps =
  {
    title: "Default",
    price: "Default",
    titleColor: "#00E676",
    priceColor: "#00E676",
    info: [],
    button_title: "GET STARTED"
  }

export default PriceCard;

const styles = StyleSheet.create({

  MainContainer: {

    flex: 1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',

  },

  priceCard_Container: {

    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#78909C',
    padding: 15,
    marginBottom: 10,
  },

  title: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  currency: {
    fontSize: 12,
    fontWeight: 'bold',
    marginRight: 10,
  },
  price: {
    fontSize: 29,
    fontWeight: 'bold',
  },

  price_Info: {
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 5,
    color: '#B0BEC5'
  },

  price_Button: {

    width: '90%',
    marginTop: 15,
    marginBottom: 10,
    backgroundColor: '#AA00FF',
    borderRadius: 4,
    padding: 17,
    flexDirection: 'row',
    justifyContent: 'center',

  },

  iconStyle: {

    width: 25,
    height: 25,
    justifyContent: 'flex-start',
    alignItems: 'center',
    tintColor: '#fff'

  },

  TextStyle: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 15
  }


});
