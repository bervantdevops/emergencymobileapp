import {Dimensions} from 'react-native';
import styles from '../../../../../native-base-theme/variables'
const {height, width} = Dimensions.get("window");

export default {
  actionsContainer: {
    backgroundColor: '#ffffff',
    flex: 1,
  },
  actionsContent: {
    flex: 3,
    marginHorizontal: 10,
  },
  currentRequestContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    marginHorizontal: 10,
  },
  searchContent: {
    flex: 1,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  menuBox: {
    flexDirection: 'column',
    width: '30%',
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 7,
    borderBottomWidth: 0,
    backgroundColor: '#FFF',
    borderColor: 'rgba(0,0,0,0.1)',
    elevation: 4,
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowOffset: {width: 0, height: 3}
  },
  icon: {
    width: 70,
    height: 70,
  },
  info: {
    marginTop: 10,
    fontSize: 15,
  },
  inactiveListeItem: {
    fontSize: 30,
  },
  activeListItem: {
    color: styles.brandSuccess,
    fontSize: 30,
  },
  actionsButtonContainer: {
    flex: 1,
    margin: 10,
    marginTop: 20,
  },
  actionsHeader: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  subscriptionWarning: {
    flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#414142',
    paddingHorizontal: 10,
    paddingVertical: 3,
  },
  subscriptionWarningText: {
    flexWrap: 'wrap',
    color: '#FFF',
    fontSize: 12,
  },
  subscribeItem: {
    flex: 1,
    marginVertical: 1,
  },
  subscriptionText: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 10,
  }
}
