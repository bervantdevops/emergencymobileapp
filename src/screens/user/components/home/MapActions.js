import React from 'react';
import { Image, ScrollView } from 'react-native';
import { View, Text, List, Thumbnail, ListItem, Left, Right, Body, Icon, Spinner, Button, Item } from 'native-base';
import styles from './mapActionStyles'
import { MyStyles, UserCard } from 'my-components'
import userService from '../../module/userService'

export default class MapActions extends React.Component {
  state = {
    categories: [],
    bLoadingCategories: false,
    selectedCategoryId: null,
  }

  componentWillMount = () => {
    const { currentRequest, onPressItem } = this.props;
    if (!this.currentRequest) {
      this._fetchCategories()
    }
  }

  _fetchCategories = () => {
    this.setState({ bLoadingCategories: true });
    userService.myCategories()
      .then(({ data }) => {
        this.setState({ categories: data });
      })
      .finally(() => {
        this.setState({ bLoadingCategories: false });
      })
  }

  _onSelectItem = (category) => {
    this.setState({ selectedCategoryId: category.id })
  }

  _onPressConfirm = () => {
    const { selectedCategoryId } = this.state;
    if (!selectedCategoryId) {
      alert("Please select a category first")
      return;
    }
    this.props.onPressItem(selectedCategoryId)
  }

  _onPressSubscribe = () => {
    this.props.onPressSubscribe();
  }

  render() {
    const { currentRequest, onPressItem } = this.props;
    return !!currentRequest ? this.renderCurrentRequest() : this.renderActions();
  }

  renderActions() {
    const { onPressItem, user, onPressSubscribe } = this.props;
    const { categories, bLoadingCategories, selectedCategoryId } = this.state;
    return (
      <View style={styles.actionsContainer}>
        {!!user && !user.plan && (
          <View style={styles.subscriptionWarning}>
          <Text  style={styles.subscriptionWarningText}>
          Seem like you have not subscribed to you tailored plans. You can get access to more services by subscribing to one of our plans

          </Text>
          <Item style={styles.subscribeItem} onPress={this._onPressSubscribe}>
            <Text style={styles.subscriptionText}>Subscribe Now</Text>
          </Item>
      </View>
        )}
        <View style={styles.actionsHeader}>
          <Text note >Select the category of help you need</Text>
        </View>
        <View style={styles.actionsContent}>
          <ScrollView>
            {categories.map((item, index) => (
              <ListItem
                style={styles.activeListItem}
                avatar
                active={item.id == selectedCategoryId}
                noBorder
                key={index}
                onPress={() => this._onSelectItem(item)}>
                <Left>
                  <Thumbnail source={{ uri: item.iconUrl }} />
                </Left>
                <Body>
                  <Text>{item.name}</Text>
                  <Text note>{item.cost ? item.cost : 'FREE'}</Text>
                </Body>
                <Right>
                  <Icon
                    style={item.id == selectedCategoryId ? styles.activeListItem : styles.inactiveListeItem}
                    name="ios-checkmark-circle" />
                </Right>
              </ListItem>
            ))}
          </ScrollView>
        </View>
        <View style={styles.actionsButtonContainer}>
          <Button primary block onPress={this._onPressConfirm}>
            <Text>Confirm Request</Text>
          </Button>
        </View>
      </View>
    );
  }

  renderCurrentRequest() {
    const { onCancelRequest, currentRequest } = this.props;
    return (
      <View style={styles.currentRequestContainer}>
        <View style={styles.searchContent}>
          {!!currentRequest.responder && <UserCard user={currentRequest.responder} />}
          <Text style={[MyStyles.Styles.textCenter, { marginVertical: 20 }]}>
            {this.renderRequestStatus(currentRequest)}
          </Text>
          <Button iconRight block style={MyStyles.Styles.bgPrimary}
            onPress={() => onCancelRequest(currentRequest.hashId)}>
            <Text style={MyStyles.Styles.textWhite}>Cancel Request</Text>
          </Button>
        </View>
      </View>
    );
  }

  renderRequestStatus(request) {
    switch (request.status) {
      case 'evacuating':
        return "Evacuation in progress";
        break;
      case 'dispatched':
        return `Our resonder has been dispatched to your location. Sit tight`;
      default:
        return "Dispatching emergency signal ..."
        break;
    }
  }
}
