import React from 'react'
import { Platform, StyleSheet, Image, Alert, Dimensions } from 'react-native'
import {
  Container, Header, Left, Right, Title, Body, Content, View, Text,
  Button, Item, Icon, Spinner, Fab, List
} from 'native-base';
import styles from './homeStyles'
import mapStyle from './mapStyles'
import userService from '../../module/userService'
import MapViewDirections from 'react-native-maps-directions';
import { Notifications } from 'expo';
import Constants from 'expo-constants';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import MapView from 'react-native-maps'
import { Helpers, Theme } from "my-components";
import MapActions from "./MapActions";
import OverlaySpinner from 'react-native-loading-spinner-overlay';
import Pusher from 'pusher-js/react-native';
import * as helpers from '../../../../libs/helpers'
import RiderIcon from '../../../../components/RiderIcon';

const mapKey = 'AIzaSyBhIEqmRDfLgXu51IzExZDC5A2kh6xwBic';
const { width, height } = Dimensions.get('window');

Pusher.logToConsole = true;
const GEOLOCATION_OPTIONS = { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceInterval: 1 };
export default class Home extends React.Component {

  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: null,
  };

  state = {
    loading: false,
    location: null,
    errorMessage: null,
    error: null,
    refreshing: false,
    bRequesting: false,
    bCancelling: false,
    distance: null,
    duration: null,

  };
  channel = null;
  my_channel = null;

  constructor(props) {
    super(props)
    // this.channel =  pusher.subscribe('Emergencies');
    this.mapRef = null;
  }

  componentDidMount = () => {

    this.my_channel.bind('client-driver-response', ({ relocation }) => {
      if (!!relocation) {
        this.props.updateResponderLocation({
          lat: relocation.latitude,
          lng: relocation.longitude,
          ...relocation
        })
      }

    })
  };

  componentWillMount() {
    if (Platform.OS === 'android' && !Constants.isDevice) {
      this.setState({
        errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
      });
    } else {
      this._getLocationAsync();
    }


    const { user, currentRequest } = this.props;
    this.pusher = helpers.pusher(user.accessToken);
    this.my_channel = this.pusher.subscribe(`private-User.${user.id}`);

    this.my_channel.bind('emergency.dispatched', ({ emergency }) => {
      this.props.updateRequest(emergency);
    })

    this.my_channel.bind('emergency.started', ({ emergency }) => {
      this.props.updateRequest(emergency);
    })

    this.my_channel.bind('emergency.completed', ({ emergency }) => {

      Alert.alert(
        "Evacuation Status",
        "Evacuation completed successfully.",
        [
          {
            text: 'Okay',
            onPress: () => this.props.updateRequest(null)
          },
        ],
        { cancelable: false }
      );

    })
  }


  componentWillUnmount() {
    this._notificationSubscription && this
      ._notificationSubscription
      .remove();
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        locationPermission: false,
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    location.region = Helpers.regionFrom(location.coords);
    this.setState({ location });
  };

  locationChanged = (location) => {
    region = {
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      latitudeDelta: 0.1,
      longitudeDelta: 0.05,
    },
      this.setState({ location, region })

  }

  makeRequest = (category_id) => {
    const { location } = this.state;
    const payload = { latitude: location.region.latitude, longitude: location.region.longitude, category_id };

    this.setState({ bRequesting: true });

    this.props.makeRequest(payload)
      .then(() => {

      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            Helpers.showToast({ type: 'error', message: ex.response.data.message });
            break;
          case 430:
            Helpers.showToast({ type: 'error', message: ex.response.data.message });
            break;
          case 500:
            Helpers.showToast({ type: 'error', message: ex.response.data.message });
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({ bRequesting: false });
      });
  };

  _cancelRequest = (request_id) => {
    this.setState({ bCancelling: true });

    this.props.cancelRequest(request_id)
      .then(() => {

      })
      .catch((ex) => {
        switch (ex.response.status) {
          case 422:
            console.log("422 ===> ", ex.response.data);
            Helpers.showToast({ type: 'error', message: ex.response.data.message });
            break;
          case 430:
            Helpers.showToast({ type: 'error', message: ex.response.data.message });
            break;
          case 500:
            Helpers.showToast({ type: 'error', message: ex.response.data.message });
            break;
          default:
            console.log(ex);
            break;
        }
      })
      .finally(() => {
        this.setState({ bCancelling: false });
      });
  };

  _onPressSubscribe = () => {
    this.props.navigation.navigate("Pricing", {})
  }

  render() {
    const { location, bRequesting, bCancelling } = this.state;
    return (
      <Container style={styles.container}>
        <Header iosBarStyle="dark-content" androidStatusBarColor={'#F1F3F5'}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Home</Title>
          </Body>
          <Right />
        </Header>
        <OverlaySpinner
          visible={bCancelling || bRequesting}
          textContent={'Loading ....'}
          textStyle={{ color: '#FFF' }} />
        <View style={{ flex: 1 }}>
          {!!location && this._renderMap()}
          {!location && this._renderSpinner()}
        </View>
      </Container>
    )
  }

  _renderSpinner = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Spinner color={'blue'} />
        <Text>Getting things ready. One minute ...</Text>
      </View>
    )
  };

  _renderMap = () => {
    const { location, duration } = this.state;
    const { currentRequest, user, responderLocation } = this.props;
    return (
      <View style={styles.mapContainer}>
        <MapView
          ref={(ref) => {
            this.mapRef = ref
          }}
          style={[styles.map, { flex: !!currentRequest ? 2 : 1 }]}
          initialRegion={location.region}
          provider={"google"}
          loadingEnabled={true}
          customMapStyle={mapStyle}
          showsUserLocation={true}
          showsCompass={true}
          showsScale={true}
          showsTraffic={false}
          toolbarEnabled={true}

          showsPointsOfInterest={true}>
          {!!currentRequest && !!responderLocation && !!currentRequest.responder && <MapViewDirections
            destination={responderLocation}
            origin={currentRequest.location}
            apikey={mapKey}
            strokeWidth={4}
            strokeColor={Theme.brandPrimary}
            optimizeWaypoints={true}
            onReady={result => {
              this.setState({
                distance: result.distance,
                duration: result.duration,
              })

              this.mapRef.fitToCoordinates(result.coordinates, {
                edgePadding: {
                  right: 200,
                  bottom: (height / 20),
                  left: (width / 20),
                  top: (height / 20),
                }
              });
            }}
          />}
          {!!currentRequest && !!responderLocation && !!currentRequest.responder && (
            <MapView.Marker
              ref={ref => { this.mrkResponder = ref; }}
              key="destination"
              coordinate={responderLocation}>
              <RiderIcon icon={currentRequest.category.iconUrl} duration={duration} />
            </MapView.Marker>
          )}
        </MapView>
        <View style={[styles.mapActionsContainer]}>
          <MapActions
            currentRequest={currentRequest}
            user={user}
            onCancelRequest={this._cancelRequest}
            onPressSubscribe={this._onPressSubscribe}
            onPressItem={this.makeRequest} />
        </View>
      </View>

    )
  };


  async registerForPushNotificationsAsync() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
    let token = await Notifications.getExpoPushTokenAsync();

    // const user = await helpers.getItemFromState('user');
    //
    // // Get the token that uniquely identifies this device
    //
    //
    // if (user.push_token && user.push_token == token)
    //   return;

    return userService.registerPushToken(token)
      .then(data => {

      })
  }

  _handleNotification = (notification) => {
    this.setState({ notification: notification });
    // console.log("Notification: ", notification)
  };
}

