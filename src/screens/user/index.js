import Home from './containers/HomeContainer'
import Contacts from './containers/ContactContainer'
import Profile from './containers/ProfileContainer'
import UserProfile from './containers/UserProfileContainer'
import Settings from './containers/SettingsContainer'
import Pricing from './containers/PricingContainer'

module.exports = {
  Home, Profile, UserProfile, Settings, Contacts, Pricing
};
