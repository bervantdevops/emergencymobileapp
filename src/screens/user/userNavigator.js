import React from "react";
import {Platform} from "react-native";
import {createBottomTabNavigator, createDrawerNavigator} from "react-navigation";
import {
  Home, Profile, UserProfile, Settings, Contacts, Pricing
} from './index';

export default createDrawerNavigator({
  Home: {screen: Home},
  Contacts: {screen: Contacts},
  Pricing: {screen: Pricing},
  // UserProfile: {screen: UserProfile},
  Settings: {screen: Settings}
}, {
  initialRouteName: 'Home',
  headerMode: 'none',
  navigationOptions: {
    header: null
  },
});
