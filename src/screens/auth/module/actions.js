import types from './types'
import authService from './authService'

export function login(username, password){
  return (dispatch, state) => {
    return authService.auth(username, password).then( data => {
      dispatch(success({user: data.data, accessToken: data.accessToken}))
    })
  };

  function success({user, accessToken}) {
    return { type: types.LOGIN, user, accessToken}
  }
}

export function register(params){
  return (dispatch, state) => {
    return authService.register(params).then( data => {
      dispatch(success({user: data.data, accessToken: data.accessToken}))
    })
  };

  function success({user, accessToken}) {
    return { type: types.LOGIN, user, accessToken}
  }
}