import types from './types';
import { createReducer} from '../../../libs/helpers';

const initial_state  = {
  user: null
};

export const user = createReducer(null, {
  [types.LOGIN](state, action) {
    let newState = {};
    newState = action.user;
    return newState;
  },
  [types.REGISTER](state, action) {
    let newState = {};
    newState = action.user;
    return newState;
  },
  [types.LOGOUT](state, action) {
    let newState = null;
    newState = null;
    return newState;
  },
});

export const accessToken = createReducer(null, {
  [types.LOGIN](state, action) {
    let newState = {};
    newState = action.accessToken;
    return newState;
  },
  [types.REGISTER](state, action) {
    let newState = {};
    newState = action.accessToken;
    return newState;
  },
  [types.LOGOUT](state, action) {
    let newState = null;
    newState = null;
    return newState;
  },
});