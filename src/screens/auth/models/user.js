import authService from '../module/authService'
import userService from '../../user/module/userService';

export default {
  state: null, // initial state
  reducers: {
    // handle state changes with pure functions
    set(state, payload) {
      return payload;
    },
    addDetails(state, payload) {
      return payload;
    },
    update(state, payload){
      return {...state, ...payload}
    }
  },
  effects: (dispatch) => ({
    async login({username, password}, rootState) {

      return authService.auth(username, password).then( data => {
        const user  = Object.assign(data.data, { accessToken: data.accessToken});
        dispatch.user.set(user);
      });
    },
    async fbLogin({token}, rootState) {
      return authService.fbLogin(token).then( data => {
        const user  = Object.assign(data.data, { accessToken: data.accessToken});
        dispatch.user.set(user);
      });
    },
    async register(payload, rootState) {
      return authService.register(payload).then( data => {
        const user  = Object.assign(data.data, { accessToken: data.accessToken});
        dispatch.user.set(user);
      });
    },
    async refresh(payload, rootState){
      return userService.myProfile().then( data => {
        const user  = data.data;
        const newState = Object.assign(rootState.user, data.data);
        dispatch.user.set(newState);

        dispatch.requests.update(data.currentRequest);
      });
    },
    async updateProfile(payload, rootState){
      return userService.updateProfile(payload).then( data => {
        const user  = data.data;
        const newState = Object.assign(rootState.user, data.data);
        dispatch.user.set(newState);
      });
    },
    async subscribe(payload, rootState){

      return userService.subscribe(payload).then( data => {
        dispatch.user.set(data.data);
      });
    },
    async logout(){
      dispatch.user.set(null)
    },
  })
}
