const styles = {
  container: {
    backgroundColor: '#FFF',
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  btnBack: {
    position: 'absolute',
    top: 20,
    left: 20
  },
  headerText: {
    marginTop: 30,
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  headerContainer: {
    flex: 2,
    position: 'relative',
    padding: 20,

  },
  footer: {
    flex: 1,
    padding: 20,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    zIndex: -1,
  },
  btnSignUp: {
    marginVertical: 10
  },
  btnSignIn: {
    marginVertical: 20
  },
  txtSignUp: {
    alignSelf: 'center',
    marginVertical: 10,
  }
};

export default styles;
