import variable from "../variables/platform";

export default (variables = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    ".note": {
      color: "#a7a7a7",
      fontSize: variables.noteFontSize
    },
    ".danger": {
      color: variables.brandDanger
    },
    ".success": {
      color: variables.brandSuccess
    },
    ".info": {
      color: variables.brandInfo
    },
    ".small": {
      fontSize: 13
    }
  };

  return textTheme;
};
