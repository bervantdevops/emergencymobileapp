import variable from "../variables/platform";

export default (variables = variable) => {
  const pickerTheme = {
    ".note": {
      color: "#8F8E95"
    },
    '.input': {
      borderBottomWidth: 1
    },
    // width: 90,
    // marginRight: -4,
    height: variables.inputHeightBase,
    color: variables.inputColor,
    paddingLeft: 0,
    paddingRight: 5,
    flex: 1,
    fontSize: variables.inputFontSize,
    flexGrow: 1,
  };

  return pickerTheme;
};
